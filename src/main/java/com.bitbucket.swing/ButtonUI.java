package com.bitbucket.swing;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;

public class ButtonUI extends BasicButtonUI {

    public void installUI ( JComponent c ) {

        super.installUI ( c );

        AbstractButton button = ( AbstractButton ) c;
        button.setOpaque ( false );
        button.setFocusable ( true );
        button.setMargin ( new Insets( 0, 0, 0, 0 ) );

       // button.setBorder ( BorderFactory.createEmptyBorder ( 4, 4, 4, 4 ) );
        button.setBorder(UIManager.getBorder("Button.Border"));
    }

    public void paint ( Graphics g, JComponent c ) {

        Graphics2D g2d = ( Graphics2D ) g;
        g2d.setRenderingHint ( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );

        AbstractButton button = ( AbstractButton ) c;
        ButtonModel buttonModel = button.getModel();
        button.setRolloverEnabled(true);

        // Бордер кнопки
        g2d.setPaint (c.getForeground());

        g2d.drawRoundRect ( 0, 0, c.getWidth () - 1, c.getHeight () - 1, 18, 18 );

        // Изменение цвета кнопки в зависимости от состояния
        if ( buttonModel.isArmed() ) {
            g2d.setPaint(new Color(100, 110, 160));//Цвет фона при нажатой кнопке
            c.setForeground(Color.BLUE);//Цвет надписи при нажатой кнопке
        }else if (buttonModel.isRollover()) {
            g.setColor(new Color(130, 150, 235));//Цвет фона при наведении курсора на кнопку
            c.setForeground(Color.BLACK); // Цвет надписи при наведении курсора на кнопку
        } else{
            g2d.setPaint (new Color(50, 40, 241) ); //Цвет фона
            c.setForeground(Color.WHITE); //Цвет надписи
        }

        // Формой кнопки будет закруглённый прямоугольник
        g2d.fillRoundRect ( 0, 0, c.getWidth (), c.getHeight (), 20, 20 );

        super.paint ( g, c );
    }

    public static ComponentUI createUI (JComponent c ) {
        // Создаём инстанс нашего UI
        return new ButtonUI();
    }
}