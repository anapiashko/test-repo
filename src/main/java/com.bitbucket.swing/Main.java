package com.bitbucket.swing;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel (LookAndFeel.class.getCanonicalName());

        SimpleGUI simpleGUI = new SimpleGUI();
        simpleGUI.setVisible(true);
    }
}
