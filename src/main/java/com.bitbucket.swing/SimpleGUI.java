package com.bitbucket.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleGUI extends JFrame {
    private JButton button = new JButton("Press");
    private JButton buttonSave = new JButton("Save");
    private JTextField input = new JTextField("", 5);
    private JTextField listRow = new JTextField("", 5);
    private JLabel label = new JLabel("Input : ");
    private JRadioButton radio1 = new JRadioButton("Select this");
    private JRadioButton radio2 = new JRadioButton("Select that");
    private JCheckBox check = new JCheckBox("Check", false);
    private String[] strings = {"Normal", "Fine", "Not bad"};
    JComboBox<String> list;

    public SimpleGUI() {
        super("Simple example");
        this.setBounds(100, 100, 450, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        //container.setLayout(new GridLayout(5, 2, 2, 2));
        container.setLayout(new VerticalLayout());

        list = new JComboBox(strings);
        list.setSelectedIndex(1);

        container.add(list);

        container.add(label);
        container.add(input);

        ButtonGroup group = new ButtonGroup();
        group.add(radio1);
        group.add(radio2);

        container.add(radio1);
        radio1.setSelected(true);

        container.add(radio2);

        container.add(check);

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String message = "";
                message += "Button was pressed\n";
                message += "In ComboBox : " + strings[list.getSelectedIndex()] +"\n";
                message += "Text is " + input.getText() + "\n";
                message += (radio1.isSelected() ? "Radio #1" : "Radio #2") + " is selected\n";
                message += "Checkbox is " + (check.isSelected() ? "checked" : "unchecked");
                JOptionPane.showMessageDialog(null, message, "Output", JOptionPane.PLAIN_MESSAGE);
            }
        });

        button.setPreferredSize(new Dimension(95, 30));
        container.add(button);
    }
}

