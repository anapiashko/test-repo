package com.bitbucket.swing;

import javax.swing.*;
import javax.swing.plaf.basic.BasicLookAndFeel;

public class LookAndFeel extends BasicLookAndFeel {

    public String getDescription () {
        // Описание LaF
        return "Cross-platform Java Look and Feel";
    }

    public String getName () {
        // Имя LaF
        return "LookAndFeel";
    }

    public String getID () {
        // Уникальный идентификатор LaF
        return getName ();
    }

    public boolean isNativeLookAndFeel () {
        // Привязан ли данный LaF к текущей платформе (является ли его нативной имплементацией)
        return false;
    }

    public boolean isSupportedLookAndFeel () {
        // Поддерживается ли данный LaF на текущей платформе
        return true;
    }

    protected void initClassDefaults ( UIDefaults table ) {
        // По прежнему оставляем дефолтную инициализацию, так как мы пока что не реализовали все
        // различные UI-классы для J-компонентов
        super.initClassDefaults ( table );

        // А вот, собственно, самое важное
        table.put(  "Button.Border",  new RoundedBorder(19) );
        table.put ( "ButtonUI", ButtonUI.class.getCanonicalName() );
    }
}